# manages client configuration
class sshd::client(
  Boolean
    $shared_ip       = false,
  String
    $ensure_version  = 'installed',
  Boolean
    $manage_firewall = false,
  Boolean
    $hardened        = false,
) {
  case $facts['os']['name'] {
    'Debian', 'Ubuntu': { include sshd::client::debian }
    default: {
      case $facts['kernel'] {
        'Linux': { include sshd::client::linux }
        default: { include sshd::client::base }
      }
    }
  }

  if $manage_firewall {
    include firewall::rules::out::ssh
  }
}
