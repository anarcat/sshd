# collect authorized_keys stored using authorized_key_add
define sshd::authorized_key_collect(
  String $target_user,
  String $collect_tag,
  Enum['present','absent'] $ensure = 'present',
  Optional[String] $path = "/home/${target_user}/.ssh/authorized_keys",
) {
  concat { $path:
    ensure         => $ensure,
    warn           => '# This file is maintained with puppet',
    ensure_newline => true,
  }
  if $ensure == 'present' {
    Concat::Fragment <<| tag == "ssh::authorized_key::fragment::${collect_tag}" |>> {
      target  => $path,
    }
  }
}
